# -*- mode: ruby -*-
# vi: set ft=ruby :

# Define a hash with server names, IP and SSH port to use in VMs
nodes = Hash['puppet-server' => '192.168.33.10:2200', 'puppet-node01' => '192.168.33.11:2201', 
             'puppet-node02' => '192.168.33.12:2202']
proxy = true # Define proxy usage (true|false)

# Iterates the nodes hash
nodes.each do |name, sock|

  Vagrant.configure(2) do |config|
  
      # Define the nodes configuration doing a iteration 
      config.vm.define name do |node|

          # Split IP and port from hash value
          ip = sock.split(":").first
          port = sock.split(":").last
          # Join server name to ".test" to create FQDN
          host = name + ".test"
          
          # Choice the some options according the type of the node 
          if name == 'puppet-server' then
            # If Puppet Server
            mem = "3096"
            pkgs = "puppetserver puppet-agent ntp"
            srv = "sudo systemctl start ntpd; sudo systemctl start puppetserver"
            hosts_file = "#{ip} #{host} #{name} puppet"
          else
            # If Puppet Client
            mem = "512"
            pkgs = "puppet-agent ntp"
            srv = "sudo systemctl start ntpd"
          end

          # Config the VM
          node.vm.box = "puppetlabs/centos-7.0-64-nocm"
          node.vm.hostname = host
          node.vm.network "private_network", ip: ip
          node.vm.network "forwarded_port", guest: 22, host: 2222, id: "ssh", disabled: true
          node.vm.network "forwarded_port", guest: 22, host: port.to_i, auto_correct: true
	        node.vm.provider "virtualbox" do |vb|
              vb.memory = mem   
	            vb.name = name
	        end

          # Proxy configuration for the nodes
          if Vagrant.has_plugin?("vagrant-proxyconf") and proxy == true
              config.proxy.http     = "http://proxy.houston.hp.com:8080/"
              config.proxy.https    = "http://proxy.houston.hp.com:8080/"
              config.proxy.no_proxy = "localhost,127.0.0.1"
          end

          # Provisioning the VM using shell mode
          node.vm.provision "shell", inline: <<-SHELL
              sudo rpm -Uvh https://yum.puppetlabs.com/puppetlabs-release-pc1-el-7.noarch.rpm
              sudo yum update -y 
              sudo yum install #{pkgs} -y
              echo "#{hosts_file}" >> /etc/hosts
              sudo /opt/puppetlabs/bin/puppet cert generate #{host} --dns_alt_names=#{name}
              echo "[agent]\ncertname = #{host} >> /etc/puppetlabs/puppet/puppet.conf"
              sudo /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
              #{srv}
              echo "PATH=$PATH:/opt/puppetlabs/bin >> /etc/profile"
          SHELL
      end
  end
end